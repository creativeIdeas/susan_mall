package cn.net.susan;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

/**
 * job启动类
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/1/3 下午3:44
 */
@EnableScheduling
@SpringBootApplication(scanBasePackages = {"cn.net.susan"})
public class JobApplication {

    public static void main(String[] args) {
        SpringApplication.run(JobApplication.class, args);
    }
}
