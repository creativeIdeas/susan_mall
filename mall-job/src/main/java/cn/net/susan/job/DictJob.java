package cn.net.susan.job;

import cn.net.susan.service.sys.DictService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * 数据字典job
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/3/27 下午5:10
 */
@Slf4j
@Component
public class DictJob {

    @Autowired
    private DictService dictService;

    @Scheduled(fixedRate = 300000)
    public void run() {
        dictService.refreshDict();
    }
}
