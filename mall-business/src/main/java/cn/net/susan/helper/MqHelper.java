package cn.net.susan.helper;

import cn.hutool.json.JSON;
import cn.hutool.json.JSONUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * mq帮助类
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/3/10 下午4:27
 */
@Slf4j
@Component
public class MqHelper {

    @Autowired
    private RabbitTemplate rabbitTemplate;


    /**
     * 发生MQ消息
     *
     * @param routingKey 路由key
     * @param message    消息
     */
    public void send(String routingKey, Message message) {
        rabbitTemplate.send(routingKey, message);
    }

    /**
     * 发生MQ消息
     *
     * @param exchange   交换机
     * @param routingKey 路由key
     * @param message    消息
     */
    public void send(String exchange, String routingKey, Object message) {
        try {
            rabbitTemplate.convertAndSend(exchange, routingKey, message);
            log.info("消息发送成功, exchange:{},routingKey:{},message:{}", exchange, routingKey, message);
        } catch (Exception e) {
            log.error("消息发送失败，原因：", e);
        }
    }
}
