package cn.net.susan.mapper.common;

import cn.net.susan.entity.common.CommonNotifyConditionEntity;
import cn.net.susan.entity.common.CommonNotifyEntity;
import java.util.List;

/**
 * 通知 mapper
 *
 * @author lisu342 该项目是知识星球：java突击队 的内部项目
 * @date 2024-02-06 10:45:26
 */
public interface CommonNotifyMapper {
	/**
     * 查询通知信息
     *
     * @param id 通知ID
     * @return 通知信息
     */
	CommonNotifyEntity findById(Long id);

	/**
     * 根据条件查询通知列表
     *
     * @param commonNotifyConditionEntity 通知信息
     * @return 通知集合
     */
	List<CommonNotifyEntity> searchByCondition(CommonNotifyConditionEntity commonNotifyConditionEntity);

	/**
	 * 根据条件查询通知数量
	 *
	 * @param commonNotifyConditionEntity 通知信息
	 * @return 通知集合
	 */
	int searchCount(CommonNotifyConditionEntity commonNotifyConditionEntity);

	/**
     * 添加通知
     *
     * @param commonNotifyEntity 通知信息
     * @return 结果
     */
	int insert(CommonNotifyEntity commonNotifyEntity);

	/**
     * 修改通知
     *
     * @param commonNotifyEntity 通知信息
     * @return 结果
     */
	int update(CommonNotifyEntity commonNotifyEntity);

	/**
     * 删除通知
     *
     * @param id 通知ID
     * @return 结果
     */
	int deleteById(Long id);

}
