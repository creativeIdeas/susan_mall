package cn.net.susan.listener;

import cn.net.susan.annotation.AsyncTask;
import cn.net.susan.factory.AsyncTaskStrategyContextFactory;
import cn.net.susan.service.task.IAsyncTask;
import org.apache.commons.collections4.MapUtils;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * 应用监听器
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/3/6 下午4:43
 */
@Component
public class TaskApplicationListener implements ApplicationListener<ContextRefreshedEvent> {
    @Override
    public void onApplicationEvent(ContextRefreshedEvent event) {
        ApplicationContext applicationContext = event.getApplicationContext();
        Map<String, Object> beansWithMap = applicationContext.getBeansWithAnnotation(AsyncTask.class);
        int initSize = MapUtils.isEmpty(beansWithMap) ? 0 : beansWithMap.size();

        Map<Integer, IAsyncTask> handlerMap = new HashMap<>(initSize);
        if (MapUtils.isNotEmpty(beansWithMap)) {
            beansWithMap.forEach((beanName, object) -> {
                AsyncTask taskTypeEnum = object.getClass().getAnnotation(AsyncTask.class);
                if (Objects.nonNull(taskTypeEnum)) {
                    handlerMap.put(taskTypeEnum.value().getValue(), (IAsyncTask) object);
                }
            });

            AsyncTaskStrategyContextFactory.getInstance().initAsyncTaskMap(handlerMap);
        }
    }
}
