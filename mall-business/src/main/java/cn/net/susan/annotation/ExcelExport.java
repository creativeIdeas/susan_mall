package cn.net.susan.annotation;

import cn.net.susan.enums.ExcelBizTypeEnum;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Excel数据导出注解
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/1/29 下午4:44
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ExcelExport {

    /**
     * 业务类型
     *
     * @return 业务类型
     */
    ExcelBizTypeEnum value();
}
