package cn.net.susan.service.email;

import cn.net.susan.entity.email.RemoteLoginEmailEntity;
import cn.net.susan.entity.sys.UserEntity;
import cn.net.susan.util.DateFormatUtil;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;

import javax.mail.MessagingException;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * 异地登录邮件服务
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/3/5 上午10:06
 */
@Service
public class RemoteLoginEmailService {

    @Autowired
    private IEmailService emailService;
    @Autowired
    private FreeMarkerConfigurer freeMarkerConfigurer;


    /**
     * 发生预警邮件
     *
     * @param emailEntity
     * @throws IOException
     * @throws TemplateException
     * @throws MessagingException
     */
    public void sendEmail(RemoteLoginEmailEntity emailEntity) throws IOException, TemplateException, MessagingException {
        Template template = freeMarkerConfigurer.getConfiguration().getTemplate("remote_login_email.html");
        Map<String, Object> model = new HashMap<>(1);
        model.put("emailEntity", emailEntity);
        String templateHtml = FreeMarkerTemplateUtils.processTemplateIntoString(template, model);
        emailService.sendHtmlEmail(emailEntity.getEmail(), "账号异地登录提醒", templateHtml);
    }
}
