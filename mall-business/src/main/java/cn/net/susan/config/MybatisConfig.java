package cn.net.susan.config;

import cn.net.susan.mybatis.UserInterceptor;
import org.mybatis.spring.boot.autoconfigure.ConfigurationCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * Mybatis配置
 *
 * @author 苏三，该项目是知识星球：java突击队 的内部项目
 * @date 2024/3/29 下午3:04
 */
@Configuration
public class MybatisConfig {

    @Bean
    public ConfigurationCustomizer configurationCustomizer() {
        return new ConfigurationCustomizer() {
            @Override
            public void customize(org.apache.ibatis.session.Configuration configuration) {
                configuration.addInterceptor(new UserInterceptor());
            }
        };
    }
}
